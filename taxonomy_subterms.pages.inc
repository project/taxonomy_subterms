<?php

/**
 * @file
 * helper functions for taxonomy_subterms
 */

/**
 * Get subterms tid and call the original taxonomy/term/% callback.
 *
 * @param $tid
 *   The TID of the term to call a callback for.
 * @param $callback
 *   The name of the original callback function.
 * @param $file
 *   The file the original term callback is in.
 * @param $filepath
 *   The path to $file.
 * @param ...
 *   Additional arguments to pass on to the callback.
 *
 * @ return
 *   The return value of the original callback function.
 */
function taxonomy_subterms_term_page($tid, $callback, $file, $filepath) {
  if (is_file($filepath .'/'. $file)) {
    require_once($filepath .'/'. $file);
  }
  $arguments = array_slice(func_get_args(), 4);
  $map=FALSE;
  $terms = taxonomy_terms_parse_string($tid);
  if (count($terms['tids'])==1) {
    //only for single term page
    $i=array_search($tid, $arguments);
    $i++;
    if (!isset($arguments[$i])) {
      //only if no depth in URL (and op = page)
      $term=taxonomy_get_term($terms['tids'][0]);
      $vocab=taxonomy_vocabulary_load($term->vid);
      
      $depth=$term->taxonomy_subterms;
      if ($depth==TAXONOMY_SUBTERMS_INHERIT) {
        $depth=$vocab->taxonomy_subterms;
      }
      if ($depth==TAXONOMY_SUBTERMS_INFINITY) {
        $depth='all';
      }
      if ($depth!=TAXONOMY_SUBTERMS_DISABLE) {
        $arguments[$i]=$depth;
      }
      
      $map=$term->taxonomy_subterms_map;
      if ($depth==TAXONOMY_SUBTERMS_INHERIT) {
        $map=$vocab->taxonomy_subterms_map;
      }
      if ($map) {
        $map = taxonomy_get_tree($term->vid, $tid, -1, 1);
      }
    }
  }
  
  // call "normal" callback
  $normal_output = call_user_func_array($callback, $arguments);

  if ($map) {
    $termsmap_output=theme('taxonomy_subterms_termsmap', $map);
    if (variable_get('taxonomy_subterms_subterms_map_position', 'before') == 'before') {
      $output = $termsmap_output . $normal_output;
    }
    else {
      $output = $normal_output . $termsmap_output;
    }
  }
  else $output = $normal_output;
  return $output;
}

function theme_taxonomy_subterms_termsmap($terms)
{
  drupal_add_css(drupal_get_path('module', 'taxonomy_subterms') . '/taxonomy_subterms.css');
  $output='';
  foreach ($terms as $term)
  {
    $output .= '<li class="taxonomy-term">';
    $output .= l(check_plain($term->name), taxonomy_term_path($term));
    if ($term->description&&variable_get('taxonomy_subterms_subterms_map_description', 'no')=='yes') {
      $output .= ' <span class="taxonomy-term-description">';
      $output .= filter_xss_admin($term->description);
      $output .= '</span>';
    }
    $output .= '</li>';
  }
  return '<ul id="taxonomy-subterms-map">' . $output . '</ul>';
}

function taxonomy_subterms_select_nodes($tid, $limit, $order = 'n.sticky DESC, n.created DESC') {
  $sql = 'SELECT DISTINCT(n.nid) FROM {node} n INNER JOIN {term_node} tn ON n.vid = tn.vid WHERE tn.tid = %d AND n.status = 1 ORDER BY '. $order;
  $sql = db_rewrite_sql($sql);
  $nid = db_result(db_query_range($sql, array($tid), 0, $limit));
  if ($nid) return node_load($nid);
  return NULL;
}
